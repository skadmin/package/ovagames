<?php

declare(strict_types=1);

namespace Skadmin\Ovagames\Doctrine\SliderItem;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\Ovagames\Doctrine\Slider\Slider;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Table(name: 'ovagames_slider_item')]
#[ORM\HasLifecycleCallbacks]
class SliderItem
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Description;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;

    #[ORM\Column]
    private string $url = '';

    #[ORM\Column]
    private string $urlText = '';

    #[ORM\Column]
    private string $preTitle = '';

    #[ORM\Column]
    private string $title = '';

    #[ORM\Column]
    private int $score = 0;

    #[ORM\Column(nullable: true)]
    private ?string $imageOne = '';

    #[ORM\Column(nullable: true)]
    private ?string $imageTwo = '';

    #[ORM\ManyToOne(targetEntity: Slider::class, inversedBy: 'items')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Slider $slider;

    public function update(Slider $slider, string $name, string $description, bool $isActive, string $url, string $urlText, string $preTitle, string $title, int $score, ?string $imagePreview, ?string $imageOne, ?string $imageTwo): void
    {
        $this->slider      = $slider;
        $this->name        = $name;
        $this->description = $description;
        $this->isActive    = $isActive;

        $this->url      = $url;
        $this->urlText  = $urlText;
        $this->preTitle = $preTitle;
        $this->title    = $title;
        $this->score    = $score;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }

        if ($imageOne !== null && $imageOne !== '') {
            $this->imageOne = $imageOne;
        }

        if ($imageTwo === null || $imageTwo === '') {
            return;
        }

        $this->imageTwo = $imageTwo;
    }

    public function getSlider(): Slider
    {
        return $this->slider;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getUrlText(): string
    {
        return $this->urlText;
    }

    public function getPreTitle(): string
    {
        return $this->preTitle;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getScore(): int
    {
        return $this->score;
    }

    public function getImageOne(): ?string
    {
        if ($this->imageOne !== null) {
            return $this->imageOne === '' ? null : $this->imageOne;
        }

        return $this->imageOne;
    }

    public function getImageTwo(): ?string
    {
        if ($this->imageTwo !== null) {
            return $this->imageTwo === '' ? null : $this->imageTwo;
        }

        return $this->imageTwo;
    }

}
