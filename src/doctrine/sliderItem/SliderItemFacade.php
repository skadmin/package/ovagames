<?php

declare(strict_types=1);

namespace Skadmin\Ovagames\Doctrine\SliderItem;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Ovagames\Doctrine\Slider\Slider;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;

final class SliderItemFacade extends Facade
{
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = SliderItem::class;
    }

    /**
     * @param array<mixed> $additionalSettings
     */
    public function create(Slider $slider, string $name, string $description, bool $isActive, string $url, string $urlText, string $preTitle, string $title, int $score, ?string $imagePreview, ?string $imageOne, ?string $imageTwo): SliderItem
    {
        return $this->update(null, $slider, $name, $description, $isActive, $url, $urlText, $preTitle, $title, $score, $imagePreview, $imageOne, $imageTwo);
    }

    /**
     * @param array<mixed> $additionalSettings
     */
    public function update(?int $id, Slider $slider, string $name, string $description, bool $isActive, string $url, string $urlText, string $preTitle, string $title, int $score, ?string $imagePreview, ?string $imageOne, ?string $imageTwo): SliderItem
    {
        $sliderItem = $this->get($id);
        $sliderItem->update($slider, $name, $description, $isActive, $url, $urlText, $preTitle, $title, $score, $imagePreview, $imageOne, $imageTwo);

        if (! $sliderItem->isLoaded()) {
            $sliderItem->setSequence($this->getValidSequence());
        }

        $this->em->persist($sliderItem);
        $this->em->flush();

        return $sliderItem;
    }

    public function get(?int $id = null): SliderItem
    {
        if ($id === null) {
            return new SliderItem();
        }

        $sliderItem = parent::get($id);

        if ($sliderItem === null) {
            return new SliderItem();
        }

        return $sliderItem;
    }

    /**
     * @return SliderItem[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function getModel(?string $table = null): QueryBuilder
    {
        return parent::getModel($table)
            ->orderBy('a.sequence');
    }

    public function getModelForSlider(Slider $slider): QueryBuilder
    {
        $repository = $this->em
            ->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('slider', $slider));

        return $repository->createQueryBuilder('a')
            ->orderBy('a.sequence')
            ->addCriteria($criteria);
    }
}
