<?php

declare(strict_types=1);

namespace Skadmin\Ovagames\Doctrine\Slider;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class SliderFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Slider::class;
    }

    public function create(string $name, string $type, bool $isActive): Slider
    {
        return $this->update(null, $name, $type, $isActive);
    }

    public function update(?int $id, string $name, string $type, bool $isActive): Slider
    {
        $slider = $this->get($id);
        $slider->update($name, $type, $isActive);

        $this->em->persist($slider);
        $this->em->flush();

        return $slider;
    }

    public function get(?int $id = null): Slider
    {
        if ($id === null) {
            return new Slider();
        }

        $slider = parent::get($id);

        if ($slider === null) {
            return new Slider();
        }

        return $slider;
    }

    /**
     * @return Slider[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria);
    }

    public function findByCode(string $code): ?Slider
    {
        $criteria = ['code' => $code];

        $slider = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
        assert($slider instanceof Slider || $slider === null);

        return $slider;
    }
}
