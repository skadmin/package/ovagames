<?php

declare(strict_types=1);

namespace Skadmin\Ovagames\Doctrine\Slider;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Ovagames\Doctrine\SliderItem\SliderItem;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Table(name: 'ovagames_slider')]
#[ORM\HasLifecycleCallbacks]
class Slider
{
    public const TYPE_MAIN = 'main';
    public const TYPE_GAME = 'game';

    public const TYPES = [
        self::TYPE_MAIN => 'ovagames.slider.type.main',
        self::TYPE_GAME => 'ovagames.slider.type.game',
    ];

    use Entity\Id;
    use Entity\Name;
    use Entity\IsActive;
    use Entity\Code;
    use Entity\Type;

    /** @var ArrayCollection|Collection|SliderItem[] */
    #[ORM\OneToMany(targetEntity: SliderItem::class, mappedBy: 'slider')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    private ArrayCollection|Collection|array $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function update(string $name, string $type, bool $isActive): void
    {
        $this->name     = $name;
        $this->type     = $type;
        $this->isActive = $isActive;
    }

    /**
     * @return ArrayCollection|Collection|SliderItem[]
     */
    public function getItems(bool $onlyActive = false, ?int $limit = null): ArrayCollection|Collection|array
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->where(Criteria::expr()->eq('isActive', true));
        }

        if ($limit !== null) {
            $criteria->setMaxResults($limit);
        }

        return $this->items->matching($criteria);
    }

    public function isTypeMain(): bool
    {
        return $this->type === self::TYPE_MAIN;
    }

    public function isTypeGame(): bool
    {
        return $this->type === self::TYPE_GAME;
    }
}
