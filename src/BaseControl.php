<?php

declare(strict_types=1);

namespace Skadmin\Ovagames;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'ovagames';
    public const DIR_IMAGE = 'ovagames';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-gamepad']),
            'items'   => [
                'overview-slider',
            ],
        ]);
    }
}
