<?php

declare(strict_types=1);

namespace Skadmin\Ovagames\Components\Admin;

interface IOverviewSliderFactory
{
    public function create(): OverviewSlider;
}
