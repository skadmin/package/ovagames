<?php

declare(strict_types=1);

namespace Skadmin\Ovagames\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Ovagames\BaseControl;
use Skadmin\Ovagames\Doctrine\Slider\Slider;
use Skadmin\Ovagames\Doctrine\Slider\SliderFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function boolval;
use function intval;

class OverviewSlider extends GridControl
{
    use APackageControl;
    use IsActive;

    private SliderFacade  $facade;
    private LoaderFactory $webLoader;

    public function __construct(SliderFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade    = $facade;
        $this->webLoader = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewSlider.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'ovagames.overview-slider.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        // DATA
        $dataType = array_map(fn(string $t): string => $this->translator->translate($t), Slider::TYPES);

        // GRID
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('name', 'grid.ovagames.overview-slider.name')
            ->setRenderer(function (Slider $slider): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'overview-slider-items',
                        'id'      => $slider->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($slider->getName());

                return $name;
            });
        $grid->addColumnText('type', 'grid.ovagames.overview-slider.name')
            ->setReplacement($dataType);
        $this->addColumnIsActive($grid, 'ovagames.overview');
        $grid->addColumnText('items-count', 'grid.ovagames.overview-slider.items-count')
            ->setRenderer(static function (Slider $slider): int {
                return $slider->getItems()->count();
            })->setAlign('center');

        // FILTER
        $grid->addFilterText('name', 'grid.ovagames.overview-slider.name');
        $grid->addFilterSelect('type', 'grid.ovagames.overview-slider.type', Constant::PROMTP_ARR + $dataType);
        $this->addFilterIsActive($grid, 'ovagames.overview');

        // ACTION
        $grid->addAction('overviewPhotos', 'grid.ovagames.overview-slider.action.overview-items', 'Component:default', ['id' => 'id'])
            ->addParameters([
                'package' => new BaseControl(),
                'render'  => 'overview-slider-items',
            ])->setIcon('images')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            // INLINE ADD
            $grid->addInlineAdd()
                ->setPositionTop()
                ->setText($this->translator->translate('grid.ovagames.overview-slider.action.inline-add'))
                ->onControlAdd[] = [$this, 'onInlineAdd'];
            $grid->getInlineAddPure()
                ->onSubmit[]     = [$this, 'onInlineAddSubmit'];

            // INLINE EDIT
            $grid->addInlineEdit()
                ->onControlAdd[]                        = [$this, 'onInlineEdit'];
            $grid->getInlineEditPure()->onSetDefaults[] = [$this, 'onInlineEditDefaults'];
            $grid->getInlineEditPure()->onSubmit[]      = [$this, 'onInlineEditSubmit'];
        }

        return $grid;
    }

    public function onInlineAdd(Container $container): void
    {
        $container->addText('name', 'grid.ovagames.overview-slider.name')
            ->setRequired('grid.ovagames.overview-slider.name.req');
        $container->addSelect('type', 'grid.ovagames.overview-slider.type', Slider::TYPES)
            ->setPrompt(Constant::PROMTP)
            ->setRequired('grid.ovagames.overview-slider.type.req');
        $container->addSelect('isActive', 'grid.ovagames.overview-slider.is-active', Constant::DIAL_YES_NO)
            ->setDefaultValue(Constant::YES);
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineAddSubmit(ArrayHash $values): void
    {
        $slider = $this->facade->create($values->name, $values->type, boolval($values->isActive));

        $message = new SimpleTranslation('grid.ovagames.overview-slider.action.flash.inline-add.success "%s"', [$slider->getName()]);
        $this->onFlashmessage($message, Flash::SUCCESS);
    }

    public function onInlineEdit(Container $container): void
    {
        $this->onInlineAdd($container);
    }

    public function onInlineEditDefaults(Container $container, Slider $slider): void
    {
        $container->setDefaults([
            'name'     => $slider->getName(),
            'type'     => $slider->getType(),
            'isActive' => intval($slider->isActive()),
        ]);
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineEditSubmit(int|string $id, ArrayHash $values): void
    {
        $slider = $this->facade->update(intval($id), $values->name, $values->type, boolval($values->isActive));

        $message = new SimpleTranslation('grid.ovagames.overview-slider.action.flash.inline-edit.success "%s"', [$slider->getName()]);
        $this->onFlashmessage($message, Flash::SUCCESS);
    }
}
