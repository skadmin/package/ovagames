<?php

declare(strict_types=1);

namespace Skadmin\Ovagames\Components\Admin;

interface IOverviewSliderItemsFactory
{
    public function create(int $id): OverviewSliderItems;
}
