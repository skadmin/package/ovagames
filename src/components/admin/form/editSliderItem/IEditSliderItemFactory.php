<?php

declare(strict_types=1);

namespace Skadmin\Ovagames\Components\Admin;

interface IEditSliderItemFactory
{
    public function create(?int $id = null): EditSliderItem;
}
