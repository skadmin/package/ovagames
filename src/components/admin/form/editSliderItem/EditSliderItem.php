<?php

declare(strict_types=1);

namespace Skadmin\Ovagames\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Exception;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Ovagames\BaseControl;
use Skadmin\Ovagames\Doctrine\Slider\Slider;
use Skadmin\Ovagames\Doctrine\Slider\SliderFacade;
use Skadmin\Ovagames\Doctrine\SliderItem\SliderItem;
use Skadmin\Ovagames\Doctrine\SliderItem\SliderItemFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function intval;

class EditSliderItem extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory    $webLoader;
    private SliderItemFacade $facade;
    private SliderItem       $sliderItem;
    private SliderFacade     $facadeSlider;
    private ?Slider          $slider = null;
    private ImageStorage     $imageStorage;

    public function __construct(?int $id, SliderItemFacade $facade, SliderFacade $facadeSlider, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->facadeSlider = $facadeSlider;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->sliderItem = $this->facade->get($id);

        if ($this->sliderItem->isLoaded()) {
            $this->slider = $this->sliderItem->getSlider();
        } elseif (isset($_GET['sliderId'])) {
            $this->slider = $this->facadeSlider->get(intval($_GET['sliderId']));
        }
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->sliderItem->isLoaded()) {
            return new SimpleTranslation('ovagames.edit-slider-item.title - %s', $this->sliderItem->getName());
        }

        return 'ovagames.edit-slider-item.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editSliderItem.latte');

        $template->sliderItem = $this->sliderItem;
        $template->slider     = $this->slider;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        if (! $this->sliderItem->isLoaded()) {
            $form->addHidden('sliderId', $this->slider !== null ? (string) $this->slider->getId() : '');
        }

        // INPUT
        $form->addText('name', 'form.ovagames.edit-slider-item.name')
            ->setRequired('form.ovagames.edit-slider-item.name.req');
        $form->addCheckbox('isActive', 'form.ovagames.edit-slider-item.is-active')
            ->setDefaultValue(true);

        $form->addUrl('url', 'form.ovagames.edit-slider-item.url', '');
        $form->addText('urlText', 'form.ovagames.edit-slider-item.url-text')
            ->addConditionOn($form['url'], Form::FILLED, 'form.ovagames.edit-slider-item.url-text.rule.filled.url');
        $form->addText('preTitle', 'form.ovagames.edit-slider-item.pre-title');
        $form->addText('title', 'form.ovagames.edit-slider-item.title')
            ->setRequired('form.ovagames.edit-slider-item.name.req');
        $form->addTextArea('description', 'form.ovagames.edit-slider-item.description')
            ->setRequired('form.ovagames.edit-slider-item.name.req');

        $form->addSelect('score', 'form.ovagames.edit-slider-item.title', array_combine(range(0, 5), range(0, 5)))
            ->setDefaultValue(0)
            ->setTranslator(null);

        $form->addImageWithRFM('imagePreview', 'form.ovagames.edit-slider-item.image-preview');
        $form->addImageWithRFM('imageOne', 'form.ovagames.edit-slider-item.image-one');
        $form->addImageWithRFM('imageTwo', 'form.ovagames.edit-slider-item.image-two');

        // BUTTON
        $form->addSubmit('send', 'form.ovagames.edit-slider-item.send');
        $form->addSubmit('sendBack', 'form.ovagames.edit-slider-item.send-back');
        $form->addSubmit('back', 'form.ovagames.edit-slider-item.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->sliderItem->isLoaded()) {
            return [];
        }

        return [
            'name'        => $this->sliderItem->getName(),
            'description' => $this->sliderItem->getDescription(),
            'isActive'    => $this->sliderItem->isActive(),
            'url'         => $this->sliderItem->getUrl(),
            'urlText'     => $this->sliderItem->getUrlText(),
            'preTitle'    => $this->sliderItem->getPreTitle(),
            'title'       => $this->sliderItem->getTitle(),
            'score'       => $this->sliderItem->getScore(),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);
        $imageOne   = UtilsFormControl::getImagePreview($values->imageOne, BaseControl::DIR_IMAGE);
        $imageTwo   = UtilsFormControl::getImagePreview($values->imageTwo, BaseControl::DIR_IMAGE);

        if (isset($values->sliderId)) {
            $this->slider = $this->facadeSlider->get(intval($values->sliderId));
        }

        if ($this->slider === null) {
            throw new Exception('Slider result not found!');
        }

        if ($this->sliderItem->isLoaded()) {
            if ($identifier !== null && $this->sliderItem->getImagePreview() !== null) {
                $this->imageStorage->delete($this->sliderItem->getImagePreview());
            }

            if ($imageOne !== null && $this->sliderItem->getImageOne() !== null) {
                $this->imageStorage->delete($this->sliderItem->getImageOne());
            }

            if ($imageTwo !== null && $this->sliderItem->getImageTwo() !== null) {
                $this->imageStorage->delete($this->sliderItem->getImageTwo());
            }

            $slider = $this->facade->update(
                $this->sliderItem->getId(),
                $this->slider,
                $values->name,
                $values->description,
                $values->isActive,
                $values->url,
                $values->urlText,
                $values->preTitle,
                $values->title,
                $values->score ?? 0,
                $identifier,
                $imageOne,
                $imageTwo
            );
            $this->onFlashmessage('form.ovagames.edit-slider-item.flash.success.update', Flash::SUCCESS);
        } else {
            $slider = $this->facade->create(
                $this->slider,
                $values->name,
                $values->description,
                $values->isActive,
                $values->url,
                $values->urlText,
                $values->preTitle,
                $values->title,
                $values->score ?? 0,
                $identifier,
                $imageOne,
                $imageTwo
            );
            $this->onFlashmessage('form.ovagames.edit-slider-item.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-slider-item',
            'id'      => $slider->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-slider-items',
            'id'      => $_POST['sliderId'] ?? $this->sliderItem->getSlider()->getId(),
        ]);
    }
}
