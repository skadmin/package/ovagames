<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220419135006 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ovagames_slider (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, code VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ovagames_slider_item (id INT AUTO_INCREMENT NOT NULL, slider_id INT DEFAULT NULL, url VARCHAR(255) NOT NULL, url_text VARCHAR(255) NOT NULL, pre_title VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, image_one VARCHAR(255) DEFAULT NULL, image_two VARCHAR(255) DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, image_preview VARCHAR(255) DEFAULT NULL, sequence INT NOT NULL, INDEX IDX_9906DFC82CCC9638 (slider_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ovagames_slider_item ADD CONSTRAINT FK_9906DFC82CCC9638 FOREIGN KEY (slider_id) REFERENCES ovagames_slider (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE ovagames_slider_item DROP FOREIGN KEY FK_9906DFC82CCC9638');
        $this->addSql('DROP TABLE ovagames_slider');
        $this->addSql('DROP TABLE ovagames_slider_item');
    }
}
